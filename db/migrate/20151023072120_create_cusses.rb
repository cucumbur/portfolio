class CreateCusses < ActiveRecord::Migration
  def change
    create_table :cusses do |t|
      t.string :badword
      t.string :replacement

      t.timestamps null: false
    end
  end
end
