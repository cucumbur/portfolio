class CreateSourceFiles < ActiveRecord::Migration
  def change
    create_table :source_files do |t|
      t.string :path
      t.integer :filesize
      t.string :filetype
      t.timestamps null: false
    end
    add_index :source_files, :path

  end
end
