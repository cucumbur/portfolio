class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.string :date
      t.string :version
      t.string :summary

      t.timestamps null: false
    end
  end
end
