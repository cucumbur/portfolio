class AddReferenceToSourceFiles < ActiveRecord::Migration
  def change
    add_reference :source_files, :project, index: true, foreign_key: true
  end
end
