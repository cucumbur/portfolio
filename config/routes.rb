Rails.application.routes.draw do
  get 'source_files/display'

  get 'file/display'

  resources :projects 
  root 'projects#index'
  
  get '/showfile/:source_file_id' => 'projects#showfile', as: :show_file
  get 'update'   => 'projects#update_from_file'
  get 'clear'   => 'projects#clear_all_projects'
  
  resources :source_files,          only: [:create, :show]
  resources :comments,               only: [:create, :show]
end
