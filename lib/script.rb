Project.all.destroy_all
#file = File.join(Rails.root, 'app', 'assets', 'svn_list1.xml')
doc = Nokogiri::XML(File.open(File.join( Rails.root, 'app', 'assets', 'svn_list1.xml' ) ) )
entries =  doc.css('entry')

entries.each do |entry|
  new_proj = Project.new
  new_proj.title = entry.css('name').text
  new_proj.date = entry.css('date').text
  new_proj.version = entry.css('commit').attribute('revision')
  new_proj.summary = "No Summary."
  new_proj.save
end