class SourceFilesController < ApplicationController
  def show
    @source_file = SourceFile.find(params[:id])
  end
end
