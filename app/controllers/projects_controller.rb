class ProjectsController < ApplicationController
  def index
    @projects = Project.all
    @comments = Comment.all
    @comment  = Comment.new
    

  end
  
  def show
    @project = Project.find(params[:id])
    @source_files = @project.source_files
  end

  def new
  end

  def create
  end
  
  def update_from_file
    
    Project.all.destroy_all
    #file = File.join(Rails.root, 'app', 'assets', 'svn_list1.xml')
    doc = Nokogiri::XML(File.open(File.join( Rails.root, 'app', 'assets', 'projects.xml' ) ) )
    entries =  doc.css('entry')
    
    entries.each do |entry|
      new_proj = Project.new
      new_proj.title = entry.css('name').text.to_s
      new_proj.date = entry.css(' date').text.to_s
      new_proj.version = entry.css('commit').attribute('revision').to_s
      new_proj.summary = "No Summary."
      new_proj.save
    end
    
    doc2 = Nokogiri::XML(File.open(File.join( Rails.root, 'app', 'assets', 'svn_list.xml' ) ) )
    entries2 =  doc2.css('entry')
    
    entries2.each do |entry|
      filetype = entry.attribute('kind')
      if (filetype.to_s == "file")
        filename = (entry.css('name').text).to_s
        filesize = (entry.css('size').text).to_s
        projectname = filename.match(/([^\/]*)\/.*/).captures[0].to_s
        file_mediatype = filename.match(/.*\.(.*)/).captures[0].to_s if filename.match(/.*\.(.*)/)
        file_mediatype = "other" unless filename.match(/.*\.(.*)/)
        case file_mediatype
          when "java", "hx", "rb"
            type_category = "Sourcecode"
          when "png", "jpg", "gif"
            type_category = "Image"
          when "txt"
            type_category = "Text"
          when "xml", "html"
            type_category = "Markup"
          else
            type_category = "Other"
        end
        proj = Project.find_by title: projectname
        proj.source_files.create!(path: filename, filesize:filesize, filetype:type_category)
      end
      
    end
    @projects = nil
    Cuss.all.destroy_all
    IO.foreach('badwords.txt') do |line|
      bad_word = line.to_s.match(/([^:]*):(.*)/).captures[0]
      good_word = line.to_s.match(/([^:]*):(.*)/).captures[1]
      Cuss.create(badword: bad_word, replacement: good_word)
    end
    
    respond_to do |format|
      format.html { redirect_to projects_path }
      format.js {render inline: "location.reload();" }
    end
  end
  
  def clear_all_projects
    Project.all.destroy_all
    Comment.all.destroy_all
    respond_to do |format|
      format.html { redirect_to projects_path }
      format.js {render inline: "location.reload();" }
    end
  end
  
  def showfile
    @source_file =  SourceFile.find(params[:source_file_id])  
    respond_to do |format|
      format.html #{ redirect_to projects_path }
      format.js  #{render  partial:'shared/display_svnfile' }
    end
  end

  private
  
end