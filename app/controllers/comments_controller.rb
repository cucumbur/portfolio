class CommentsController < ApplicationController
  
  def create
    @comment = Comment.new(comment_params)
    if @comment.save
      comment_text = @comment.content
      Cuss.all.each do |filter|
        comment_text.sub!(filter.badword, filter.replacement)
      end
      @comment.content = comment_text
      @comment.save
      flash[:success] = "Comment created!"
      @comments = Comment.all
    respond_to do |format|
        format.html { redirect_to root_path }
        format.js
    end
    else
     # render 'projects/index'
    end
  end
  
  private

    def comment_params
      params.require(:comment).permit(:content, :name)
    end
  
end
