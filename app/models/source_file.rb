class SourceFile < ActiveRecord::Base
  belongs_to :project
  default_scope { order(:path => :asc) }
end